![osti](./osti_banner.png)

# Osti

Hosts file building

## Installation

### Using homebrew

```sh
brew tap edraflame/tap https://gitlab.com/edraflame/homebrew-tap.git
brew install osti
```

### From sources:

```sh
git clone https://gitlab.com/edraflame/osti.git
cd osti
make
sudo make install
```

> You need [Crystal](https://crystal-lang.org) for building

## Usage


`osti build` or `osti` to build host file from files in `~/-osti`.

`osti list` to show list of files and their status.

Files should be in "hosts" file format. Add `#` at the beginning of the file's name to exclude it from builds.

`osti --help` to get more info.

> This tool assumes the hosts file is at `/etc/hosts` which should be fine for anything Unix, Unix-like, POSIX, and macOS.

## TODO

- [ ] show progress
- [ ] show remote fetch progress
