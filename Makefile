OUT_DIR=./bin

all: build

build:
	mkdir -p $(OUT_DIR)
	shards build --production --release --no-debug -p -o"$(OUT_DIR)/osti"
	strip ./bin/osti

install:
	cp $(OUT_DIR)/osti /usr/local/bin/

run:
	$(OUT_DIR)/osti

clean:
	rm -rf $(OUT_DIR) .crystal .deps libs
