require "http/client"
require "file_utils"
require "option_parser"

module Osti
  VERSION         = {{ `shards version #{__DIR__}`.chomp.stringify }}
  DEFAULT_COMMAND = "build"

  Dir.cd File.expand_path("~/.osti/")

  def self.display_help_and_exit
    puts <<-HELP

      \e[1mUsage:\e[0m osti <command> [file ...]

      \e[1mCommands:\e[0m
          build [<files>]                - Build hosts file, if no files are specified uses everything inside `~/.osti/`
          list                           - List files inside `~/.osti/`.
          --version                      - Print the version number.
          -h, --help                     - Shows this help message.


      HELP
    exit
  end

  OptionParser.parse! do |parser|
    parser.on("-h", "--help", "Show this help") { display_help_and_exit }
    parser.unknown_args do |args|
      case args[0]? || DEFAULT_COMMAND
      when "build"
        if args.size > 1
          args.shift
          build_from_files args
        else
          build
        end
      when "list"
        list
      when "--version"
        puts VERSION
      else
        display_help_and_exit
      end
      exit
    end
  end

  def self.gather_files : Array(String)
    files = Dir.children Dir.current

    files = files.select do |path|
      !path.includes?(".DS_Store") && !Dir.exists?(path)
    end

    files = files.sort

    return files
  end

  def self.build_from_files(files : Array(String))
    tempfile = File.tempfile("hosts") do |file|
      files.each do |x|
        if File.exists?(x)
          if x.starts_with? "#"
            puts " - #{x} is disabled SKIPPED"
          elsif x.starts_with? "remote_"
            puts " + " + x
            response = HTTP::Client.get File.read(x)
            file << response.body
          else
            puts " + " + x
            file << File.read(x)
          end
          file << "\n"
        else
          puts " - #{x} is not a valid file SKIPPED"
        end
      end
    end

    FileUtils.cp(tempfile.path, "/etc/hosts")

    tempfile.delete
  end

  def self.build
    build_from_files gather_files
  end

  def self.list
    files = gather_files

    files.each do |path|
      display_name = path
      disabled = display_name.starts_with?('#')
      display_name = display_name.lchop if disabled

      remote = display_name.starts_with? "remote_"
      display_name = display_name[7..] if remote

      display_name = display_name[...-6] if display_name.ends_with? ".hosts"

      puts "  #{disabled ? "\e[2mdisabled\e[0m" : "\e[1menabled\e[0m "}  #{remote ? "remote" : "\e[2m#{File.size(path).humanize_bytes.ljust(6)}\e[0m"}  #{display_name}"
    end
  end
end
